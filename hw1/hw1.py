import numpy as np
import math

"""
   Mirror an image about its border.

   Arguments:
      image - a 2D numpy array of shape (sx, sy)
      wx    - a scalar specifying width of the top/bottom border
      wy    - a scalar specifying width of the left/right border

   Returns:
      img   - a 2D numpy array of shape (sx + 2*wx, sy + 2*wy) containing
              the original image centered in its interior and a surrounding
              border of the specified width created by mirroring the interior
"""
def mirror_border(image, wx = 1, wy = 1):
   assert image.ndim == 2, 'image should be grayscale'
   sx, sy = image.shape
   # mirror top/bottom
   top    = image[:wx:,:]
   bottom = image[(sx-wx):,:]
   img = np.concatenate( \
      (top[::-1,:], image, bottom[::-1,:]), \
      axis=0 \
   )
   # mirror left/right
   left  = img[:,:wy]
   right = img[:,(sy-wy):]
   img = np.concatenate( \
      (left[:,::-1], img, right[:,::-1]), \
      axis=1 \
   )
   return img

"""
   Pad an image with zeros about its border.

   Arguments:
      image - a 2D numpy array of shape (sx, sy)
      wx    - a scalar specifying width of the top/bottom border
      wy    - a scalar specifying width of the left/right border

   Returns:
      img   - a 2D numpy array of shape (sx + 2*wx, sy + 2*wy) containing
              the original image centered in its interior and a surrounding
              border of zeros
"""
def pad_border(image, wx = 1, wy = 1):
   assert image.ndim == 2, 'image should be grayscale'
   sx, sy = image.shape
   img = np.zeros((sx+2*wx, sy+2*wy))
   img[wx:(sx+wx),wy:(sy+wy)] = image
   return img

"""
   Remove the border of an image.

   Arguments:
      image - a 2D numpy array of shape (sx, sy)
      wx    - a scalar specifying width of the top/bottom border
      wy    - a scalar specifying width of the left/right border

   Returns:
      img   - a 2D numpy array of shape (sx - 2*wx, sy - 2*wy), extracted by
              removing a border of the specified width from the sides of the
              input image
"""
def trim_border(image, wx = 1, wy = 1):
   assert image.ndim == 2, 'image should be grayscale'
   sx, sy = image.shape
   img = np.copy(image[wx:(sx-wx),wy:(sy-wy)])
   return img

"""
   Return an approximation of a 1-dimensional Gaussian filter.

   The returned filter approximates:

   g(x) = 1 / sqrt(2 * pi * sigma^2) * exp( -(x^2) / (2 * sigma^2) )

   for x in the range [-3*sigma, 3*sigma]
"""
def gaussian_1d(sigma = 1.0):
   width = np.ceil(3.0 * sigma)
   x = np.arange(-width, width + 1)
   g = np.exp(-(x * x) / (2 * sigma * sigma))
   g = g / np.sum(g)          # normalize filter to sum to 1 ( equivalent
   g = np.atleast_2d(g)       # to multiplication by 1 / sqrt(2*pi*sigma^2) )
   return g

"""
   CONVOLUTION IMPLEMENTATION (10 Points)

   Convolve a 2D image with a 2D filter.

   Requirements:

   (1) Return a result the same size as the input image.

   (2) You may assume the filter has odd dimensions.

   (3) The result at location (x,y) in the output should correspond to
       aligning the center of the filter over location (x,y) in the input
       image.

   (4) When computing a product at locations where the filter extends beyond
       the defined image, treat missing terms as zero.  (Equivalently stated,
       treat the image as being padded with zeros around its border).

   You must write the code for the nested loops of the convolutions yourself,
   using only basic loop constructs, array indexing, multiplication, and
   addition operators.  You may not call any Python library routines that
   implement convolution.

   Arguments:
      image  - a 2D numpy array
      filt   - a 1D or 2D numpy array, with odd dimensions

   Returns:
      result - a 2D numpy array of the same shape as image, containing the
               result of convolving the image with filt
"""
def apply_filter(arr1, arr2):
    assert arr1.shape == arr2.shape, 'array and filter should be same size'
    size = arr1.size
    sum = 0
    w, h = arr1.shape
    for x in range(w):
        for y in range(h):
            sum += arr1.item(x, y)
    #print("sum: %d, size: %d, val: %f" % (sum, size, sum/size))
    return sum / size

def conv_2d(image, filt):
   #print(image.size)
   # make sure that both image and filter are 2D arrays
   assert image.ndim == 2, 'image should be grayscale'
   filt = np.atleast_2d(filt)
   ##########################################################################
   filt_w, filt_h = filt.shape
   #print("Filter width : %d, Filter height : %d" % (filt_w, filt_h))
   padding_w = int(filt_w / 2)
   padding_h = int(filt_h / 2)
   image = mirror_border(image, padding_w, padding_h)
   orig_image = image.copy()
   img_w, img_h = image.shape
   for x in range(padding_w, img_w - padding_w):
       for y in range(padding_h, img_h - padding_h):
            extracted_array = orig_image[x-padding_w:x+padding_w+1, y-padding_h:y+padding_h+1]
            #print(x, y)
            image[x,y] = apply_filter(extracted_array, filt)
   ##########################################################################
   result = trim_border(image, padding_w, padding_h)
   #print(result.size)
   return result

"""
   GAUSSIAN DENOISING (5 Points)

   Denoise an image by convolving it with a 2D Gaussian filter.

   Convolve the input image with a 2D filter G(x,y) defined by:

   G(x,y) = 1 / sqrt(2 * pi * sigma^2) * exp( -(x^2 + y^2) / (2 * sigma^2) )

   You may approximate the G(x,y) filter by computing it on a
   discrete grid for both x and y in the range [-3*sigma, 3*sigma].

   See the gaussian_1d function for reference.

   Note:
   (1) Remember that the Gaussian is a separable filter.
   (2) Denoising should not create artifacts along the border of the image.
       Make an appropriate assumption in order to obtain visually plausible
       results along the border.

   Arguments:
      image - a 2D numpy array
      sigma - standard deviation of the Gaussian

   Returns:
      img   - denoised image, a 2D numpy array of the same shape as the input
"""
def denoise_gaussian(image, sigma = 1.0):
   ##########################################################################
   size = int(np.ceil(3.0 * sigma))
   x, y = np.mgrid[-size: size + 1, -size: size + 1]
   g = 1/math.sqrt(2 * math.pi * sigma * sigma) * np.exp(-(x * x + y * y) / (2 * sigma * sigma))
   g = g / np.sum(g)
   g = np.atleast_2d(g)
   #print(g)
   img = conv_2d(image, g)
   ##########################################################################
   return img

"""
   MEDIAN DENOISING (5 Points)

   Denoise an image by applying a median filter.

   Note:
       Denoising should not create artifacts along the border of the image.
       Make an appropriate assumption in order to obtain visually plausible
       results along the border.

   Arguments:
      image - a 2D numpy array
      width - width of the median filter; compute the median over 2D patches of
              size (2*width +1) by (2*width + 1)

   Returns:
      img   - denoised image, a 2D numpy array of the same shape as the input
"""


def denoise_median(image, width = 1):
   ##########################################################################
   filt_w, filt_h = (2*width+1, 2*width+1)
   padding_w = int(filt_w / 2)
   padding_h = int(filt_h / 2)
   image = mirror_border(image, padding_w, padding_h)
   orig_image = image.copy()
   img_w, img_h = image.shape
   for x in range(padding_w, img_w - padding_w):
       for y in range(padding_h, img_h - padding_h):
            extracted_array = orig_image[x-padding_w:x+padding_w+1, y-padding_h:y+padding_h+1]
            #print(x, y)
            image[x,y] = np.median(extracted_array)
   img = trim_border(image, padding_w, padding_h)
   ##########################################################################
   return img

"""
   SOBEL GRADIENT OPERATOR (5 Points)

   Compute an estimate of the horizontal and vertical gradients of an image
   by applying the Sobel operator.

   The Sobel operator estimates gradients dx, dy, of an image I as:

         [  1  2  1 ]
   dx =  [  0  0  0 ] (*) I
         [ -1 -2 -1 ]

         [ 1  0  -1 ]
   dy =  [ 2  0  -2 ] (*) I
         [ 1  0  -1 ]

   where (*) denotes convolution.

   Note:
      (1) Your implementation should be as efficient as possible.
      (2) Avoid creating artifacts along the border of the image.

   Arguments:
      image - a 2D numpy array

   Returns:
      dx    - gradient in x-direction at each point
              (a 2D numpy array, the same shape as the input image)
      dy    - gradient in y-direction at each point
              (a 2D numpy array, the same shape as the input image)

"""
def sobel_gradients(image):
   ##########################################################################
   """filter_x = [[1, 2, 1], [0, 0, 0], [-1, -2, -1]]
   filter_y = [[1, 0, -1], [2, 0, -2], [1, 0, -1]]
   dx = conv_2d(image, filter_x)
   dy = conv_2d(image, filter_y)"""
   dx = np.zeros(image.shape)
   dy = np.zeros(image.shape)
   for x in range(1, image.shape[0] - 1):
       for y in range(1, image.shape[1] - 1):
           Gx = 0
           Gy = 0
           p = image.item((x, y))
           Gx -= p
           Gy -= p
           p = image.item((x-1, y))
           Gx -= 2 * p
           p = image.item((x-1, y+1))
           Gx -= p
           Gy += p
           p = image.item((x, y-1))
           Gy -= 2 * p
           p = image.item((x, y+1))
           Gy += 2 * p
           p = image.item((x+1, y-1))
           Gx += p
           Gy -= p
           p = image.item((x+1, y))
           Gx += 2 * p
           p = image.item((x+1, y+1))
           Gx += p
           Gy += p
           dx[x][y] = Gx
           dy[x][y] = Gy
   ##########################################################################
   return dx, dy

"""
   NONMAXIMUM SUPPRESSION (10 Points)

   Nonmaximum suppression.

   Given an estimate of edge strength (mag) and direction (theta) at each
   pixel, suppress edge responses that are not a local maximum along the
   direction perpendicular to the edge.

   Equivalently stated, the input edge magnitude (mag) represents an edge map
   that is thick (strong response in the vicinity of an edge).  We want a
   thinned edge map as output, in which edges are only 1 pixel wide.  This is
   accomplished by suppressing (setting to 0) the strength of any pixel that
   is not a local maximum.

   Note that the local maximum check for location (x,y) should be performed
   not in a patch surrounding (x,y), but along a line through (x,y)
   perpendicular to the direction of the edge at (x,y).

   A simple, and sufficient strategy is to check if:
      ((mag[x,y] > mag[x + ox, y + oy]) and (mag[x,y] >= mag[x - ox, y - oy]))
   or
      ((mag[x,y] >= mag[x + ox, y + oy]) and (mag[x,y] > mag[x - ox, y - oy]))
   where:
      (ox, oy) is an offset vector to the neighboring pixel in the direction
      perpendicular to edge direction at location (x, y)

   Arguments:
      mag    - a 2D numpy array, containing edge strength (magnitude)
      theta  - a 2D numpy array, containing edge direction in [0, 2*pi)

   Returns:
      nonmax - a 2D numpy array, containing edge strength (magnitude), where
               pixels that are not a local maximum of strength along an
               edge have been suppressed (assigned a strength of zero)
"""

def getPerp(theta, rotation = 0):
    gridnum = int((theta + math.pi / 8.0) / (math.pi / 4.0))

    gridnum += rotation #rotation?

    while (gridnum > 8):
        gridnum -= 8

    if (gridnum == 0):
        return (1, 0)
    elif (gridnum == 1):
        return (1, 1)
    elif (gridnum == 2):
        return (0, 1)
    elif (gridnum == 3):
        return (-1, 1)
    elif (gridnum == 4):
        return (-1, 0)
    elif (gridnum == 5):
        return (-1, -1)
    elif (gridnum == 6):
        return (0, -1)
    elif (gridnum == 7):
        return (1, -1)
    elif (gridnum == 8):
        return (1, 0)
    else:
        #print(gridnum)
        assert(0), "Incorrect gridnum"

def nonmax_suppress(mag, theta):
   ##########################################################################

   nonmax = np.zeros(mag.shape)
   for x in range(1, mag.shape[0] - 1):
       for y in range(1, mag.shape[1] - 1):
           perpGrid = getPerp(theta[x, y])
           if (mag[x, y] < mag[x + perpGrid[0], y + perpGrid[1]]) or (mag[x, y] < mag[x - perpGrid[0], y - perpGrid[1]]):
               nonmax[x, y] = 0
           else:
               nonmax[x, y] = mag[x, y]

   ##########################################################################
   """
   for i in range(8):
       nonmax = np.zeros(mag.shape)
       for x in range(1, mag.shape[0] - 1):
           for y in range(1, mag.shape[1] - 1):
               perpGrid = getPerp(theta[x, y], i)
               if (mag[x, y] < mag[x + perpGrid[0], y + perpGrid[1]]) or (mag[x, y] < mag[x - perpGrid[0], y - perpGrid[1]]):
                   nonmax[x, y] = 0
               else:
                   nonmax[x, y] = mag[x, y]
       print("i: %d, sum: %f" % (i, np.sum(nonmax)))
   """
   return nonmax

"""
   CANNY EDGE DETECTOR (5 Points)

   Canny edge detector.

   Given an input image:

   (1) Compute gradients in x- and y-directions at every location using the
       Sobel operator.  See sobel_gradients() above.

   (2) Estimate edge strength (gradient magnitude) and direction.

   (3) Perform nonmaximum suppression of the edge strength map, thinning it
       in the direction perpendicular to that of a local edge.
       See nonmax_suppress() above.

   Return the original edge strength estimate (max), as well as the edge
   strength map after nonmaximum suppression (nonmax).

   Arguments:
      image    - a 2D numpy array

   Returns:
      mag      - edge strength at each pixel
                 (2D array, same shape as input)
      nonmax   - edge strength after nonmaximum suppression
                 (2D array, same shape as input)
"""
def canny(image):
   ##########################################################################
   dx, dy = sobel_gradients(image)
   mag = np.zeros(image.shape)
   theta = np.zeros(image.shape)
   for x in range(image.shape[0]):
       for y in range(image.shape[1]):
           mag[x][y] = math.sqrt((dx[x][y] * dx[x][y]) + (dy[x][y] * dy[x][y]))
           rad = math.atan2(dy[x][y], dx[x][y])
           if rad < 0:
               rad += 2 * math.pi
           theta[x][y] = rad
   nonmax = nonmax_suppress(mag, theta)
   ##########################################################################
   #print(theta[242, 39])
   return mag, nonmax
