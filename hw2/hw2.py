import numpy as np
from canny import *
import math #for value of pi

import matplotlib.pyplot

"""
   INTEREST POINT OPERATOR (12 Points Implementation + 3 Points Write-up)

   Implement an interest point operator of your choice.

   Your operator could be:

   (A) The Harris corner detector (Szeliski 4.1.1)

               OR

   (B) The Difference-of-Gaussians (DoG) operator defined in:
       Lowe, "Distinctive Image Features from Scale-Invariant Keypoints", 2004.
       https://www.cs.ubc.ca/~lowe/papers/ijcv04.pdf

               OR

   (C) Any of the alternative interest point operators appearing in
       publications referenced in Szeliski or in lecture

              OR

   (D) A custom operator of your own design

   You implementation should return locations of the interest points in the
   form of (x,y) pixel coordinates, as well as a real-valued score for each
   interest point.  Greater scores indicate a stronger detector response.

   In addition, be sure to apply some form of spatial non-maximum suppression
   prior to returning interest points.

   Whichever of these options you choose, there is flexibility in the exact
   implementation, notably in regard to:

   (1) Scale

       At what scale (e.g. over what size of local patch) do you operate?

       You may optionally vary this according to an input scale argument.

       We will test your implementation at the default scale = 1.0, so you
       should make a reasonable choice for how to translate scale value 1.0
       into a size measured in pixels.

   (2) Nonmaximum suppression

       What strategy do you use for nonmaximum suppression?

       A simple (and sufficient) choice is to apply nonmaximum suppression
       over a local region.  In this case, over how large of a local region do
       you suppress?  How does that tie into the scale of your operator?

   For making these, and any other design choices, keep in mind a target of
   obtaining a few hundred interest points on the examples included with
   this assignment, with enough repeatability to have a large number of
   reliable matches between different views.

   If you detect more interest points than the requested maximum (given by
   the max_points argument), return only the max_points highest scoring ones.

   In addition to your implementation, include a brief write-up (in hw2.pdf)
   of your design choices.

   Arguments:
      image       - a grayscale image in the form of a 2D np array
      max_points  - maximum number of interest points to return
      scale       - (optional, for your use only) scale factor at which to
                    detect interest points

   Returns:
      xs          - np array of shape (N,) containing x-coordinates of the
                    N detected interest points (N <= max_points)
      ys          - np array of shape (N,) containing y-coordinates
      scores      - np array of shape (N,) containing a real-valued
                    measurement of the relative strength of each interest point
                    (e.g. corner detector criterion OR DoG operator magnitude)
"""
def find_interest_points(image, max_points = 200, scale = 1.0):
   # check that image is grayscale
   assert image.ndim == 2, 'image should be grayscale'
   ##########################################################################

   dx, dy = sobel_gradients(image)

   #Auto-correlation matrix
   Ix2 = dx * dx
   Ixy = dy * dx
   Iy2 = dy * dy

   #larger scales for larger images
   scaling = int((image.size ** 0.5) / 480 * 2 + 1) + 2

   threshold = 6

   xs = []
   ys = []
   scores = []

   data = []

   #Loop through image
   for y in range(scaling, image.shape[0] - scaling):
       for x in range(scaling, image.shape[1] - scaling):

           #Sum of squares
           Sx2 = Ix2[y-scaling:y+1+scaling, x-scaling:x+1+scaling].sum()
           Sxy = Ixy[y-scaling:y+1+scaling, x-scaling:x+1+scaling].sum()
           Sy2 = Iy2[y-scaling:y+1+scaling, x-scaling:x+1+scaling].sum()

           #Calculate det and trace as in Szeliski equation 4.9
           det = Sx2 * Sy2 - Sxy * Sxy
           trace = Sx2 + Sy2
           score = det - 0.06 * trace * trace

           #check if score is above thresh
           if score > threshold:
               data.append([x, y, score])

   data.sort(key=lambda x: x[2])
   for i in range(max_points):
       try:
           if not isLocalMax(data[i], data[:i], scaling):
               max_points += 1
               continue
           xs.append(data[i][0])
           ys.append(data[i][1])
           scores.append(data[i][2])
       except IndexError:
           continue

   return xs, ys, scores

   ##########################################################################

def isLocalMax(datapoint, data, scaling):
    #data is pre-sorted by score, and only contains points with score larger than datapoint
    for d in data:
        if (abs(d[0] - datapoint[0]) < 2 * scaling) and (abs(d[1] - datapoint[1]) < 2 * scaling):
            return False
    return True

"""
   FEATURE DESCRIPTOR (12 Points Implementation + 3 Points Write-up)

   Implement a SIFT-like feature descriptor by binning orientation energy
   in spatial cells surrounding an interest point.

   Unlike SIFT, you do not need to build-in rotation or scale invariance.

   A reasonable default design is to consider a 3 x 3 spatial grid consisting
   of cell of a set width (see below) surrounding an interest point, marked
   by () in the diagram below.  Using 8 orientation bins, spaced evenly in
   [-pi,pi), yields a feature vector with 3 * 3 * 8 = 72 dimensions.

             ____ ____ ____
            |    |    |    |
            |    |    |    |
            |____|____|____|
            |    |    |    |
            |    | () |    |
            |____|____|____|
            |    |    |    |
            |    |    |    |
            |____|____|____|

                 |----|
                  width

   You will need to decide on a default spatial width.  Optionally, this can
   be a multiple of a scale factor, passed as an argument.  We will only test
   your code by calling it with scale = 1.0.

   In addition to your implementation, include a brief write-up (in hw2.pdf)
   of your design choices.

  Arguments:
      image    - a grayscale image in the form of a 2D np
      xs       - np array of shape (N,) containing x-coordinates
      ys       - np array of shape (N,) containing y-coordinates
      scale    - scale factor

   Returns:
      feats    - a np array of shape (N,K), containing K-dimensional
                 feature descriptors at each of the N input locations
                 (using the default scheme suggested above, K = 72)
"""

def gmagnitude(hg, vg):
    return np.sqrt(np.power(hg, 2) + np.power(vg, 2))

def gdirection(hg, vg):
    return np.rad2deg(np.arctan(vg/(hg+0.01)))%180

def HOGhistogram(direction, magnitude, bins):
    HOGhist = np.zeros(shape=(bins.size))

    for row in range(direction.shape[0]):
        for col in range(direction.shape[1]):
            dir = direction[row, col]
            mag = magnitude[row, col]

            d = np.abs(dir - bins)

            if dir < bins[0]:
                b1 = 0
                b2 = bins.size-1
            elif dir > bins[-1]:
                b1 = bins.size-1
                b2 = 0
            else:
                b1 = np.where(d == np.min(d))[0][0]
                tmp = bins[[(b1 - 1) % bins.size, (b1 + 1) % bins.size]]
                tmp2 = np.abs(dir - tmp)
                res = np.where(tmp2 == np.min(tmp2))[0][0]
                if res == 0 and b1 != 0:
                    b2 = b1-1
                else:
                    b2 = b1+1
            b1_val = bins[b1]
            b2_val = bins[b2]
            HOGhist[b1] = HOGhist[b1] + (np.abs(dir - b1_val) / 180.0 * bins.size ) * mag
            HOGhist[b2] = HOGhist[b2] + (np.abs(dir - b2_val) / 180.0 * bins.size) * mag

    return HOGhist

def extract_features(image, xs, ys, scale = 1.0):
   # check that image is grayscale
   assert image.ndim == 2, 'image should be grayscale'
   ##########################################################################
   bins = np.array([10, 30, 50, 70, 90, 110, 130, 150, 170])
   feats = []
   dx, dy = sobel_gradients(image)
   grad_mag = gmagnitude(dx, dy)
   grad_dir = gdirection(dx, dy) % 180
   for i in range(len(xs)):
       x = xs[i]
       y = ys[i]
       direction = grad_dir[x-int(4*scale):x+int(4*scale), y-int(4*scale):y+int(4*scale)]
       magnitude = grad_mag[x-int(4*scale):x+int(4*scale), y-int(4*scale):y+int(4*scale)]
       HOG = HOGhistogram(direction, magnitude, bins)
       feats.append(HOG)
   ##########################################################################
   return feats

"""
   FEATURE MATCHING (7 Points Implementation + 3 Points Write-up)

   Given two sets of feature descriptors, extracted from two different images,
   compute the best matching feature in the second set for each feature in the
   first set.

   Matching need not be (and generally will not be) one-to-one or symmetric.
   Calling this function with the order of the feature sets swapped may
   result in different returned correspondences.

   For each match, also return a real-valued score indicating the quality of
   the match.  This score could be based on a distance ratio test, in order
   to quantify distinctiveness of the closest match in relation to the second
   closest match.  It could optionally also incorporate scores of the interest
   points at which the matched features were extracted.  You are free to
   design your own criterion.

   In addition to your implementation, include a brief write-up (in hw2.pdf)
   of your design choices.

   Arguments:
      feats0   - a np array of shape (N0, K), containing N0 K-dimensional
                 feature descriptors (generated via extract_features())
      feats1   - a np array of shape (N1, K), containing N1 K-dimensional
                 feature descriptors (generated via extract_features())
      scores0  - a np array of shape (N0,) containing the scores for the
                 interest point locations at which feats0 was extracted
                 (generated via find_interest_point())
      scores1  - a np array of shape (N1,) containing the scores for the
                 interest point locations at which feats1 was extracted
                 (generated via find_interest_point())

   Returns:
      matches  - a np array of shape (N0,) containing, for each feature
                 in feats0, the index of the best matching feature in feats1
      scores   - a np array of shape (N0,) containing a real-valued score
                 for each match
"""

def match_features(feats0, feats1, scores0, scores1):
   ##########################################################################
   matches = []
   scores = []
   for a in feats0:
       best_score = 9999999
       best_score_index = -1
       for i in range(len(feats1)):
           d = 0.5 * np.sum([((p1 - p2) * (p1 - p2)) / (p1 + p2 + 1e-10) for (p1, p2) in zip(a, feats1[i])])
           #d = abs(np.sum(a) - np.sum(feats1[i]))
           #d = d * d2
           if d < best_score:
               best_score = d
               best_score_index = i
       matches.append(best_score_index)
       scores.append(best_score)
   ##########################################################################
   #print(matches)
   return matches, scores

"""
   HOUGH TRANSFORM (7 Points Implementation + 3 Points Write-up)

   Assuming two images of the same scene are related primarily by
   translational motion, use a predicted feature correspondence to
   estimate the overall translation vector t = [tx ty].

   Your implementation should use a Hough transform that tallies votes for
   translation parameters.  Each pair of matched features votes with some
   weight dependant on the confidence of the match; you may want to use your
   estimated scores to determine the weight.

   In order to accumulate votes, you will need to decide how to discretize the
   translation parameter space into bins.

   In addition to your implementation, include a brief write-up (in hw2.pdf)
   of your design choices.

   Arguments:
      xs0     - np array of shape (N0,) containing x-coordinates of the
                interest points for features in the first image
      ys0     - np array of shape (N0,) containing y-coordinates of the
                interest points for features in the first image
      xs1     - np array of shape (N1,) containing x-coordinates of the
                interest points for features in the second image
      ys1     - np array of shape (N1,) containing y-coordinates of the
                interest points for features in the second image
      matches - a np array of shape (N0,) containing, for each feature in
                the first image, the index of the best match in the second
      scores  - a np array of shape (N0,) containing a real-valued score
                for each pair of matched features

   Returns:
      tx      - predicted translation in x-direction between images
      ty      - predicted translation in y-direction between images
      votes   - a matrix storing vote tallies; this output is provided for
                your own convenience and you are free to design its format
"""

#input (x,y) and output (angle, distance), noting that the top left corner will be the origin
def cartesian2polar(x, y):
    return (np.arctan2(y, x), np.sqrt((x * x) + (y * y)))

def hough_votes(xs0, ys0, xs1, ys1, matches, scores):
   ##########################################################################
   temp = math.pi / 8.0
   votes = [[temp, [], 0], [3 * temp, [], 0], [5 * temp, [], 0], [7 * temp, [], 0], [9 * temp, [], 0], [11 * temp, [], 0], [13 * temp, [], 0], [15 * temp, [], 0]]
   xs = np.zeros(len(matches))
   ys = np.zeros(len(matches))
   for i in range(len(matches)):
       xs[i] = (xs1[matches[i]] - xs0[i])
       ys[i] = (ys1[matches[i]] - ys0[i])
   angles, distances = cartesian2polar(xs, ys)
   #print(angles)
   for i in range(len(angles)):
       if angles[i] >= votes[7][0]:
           angles[i] -= 2 * math.pi
           votes[7][1].append(scores[i])
           votes[7][2] += distances[i]
       for j in range(0, 7):
           if angles[i] < votes[j][0]:
               votes[j][1].append(scores[i])
               votes[j][2] += distances[i]
               break
   highscore = -1
   highscore_index = -1
   for i in range(len(angles)):
       if np.sum(votes[j][1]) > highscore:
           highscore = np.sum(votes[j][1])
           highscore_index = j

   avg_angle = np.average(votes[highscore_index][1])
   avg_magnitude = votes[highscore_index][2] / len(votes[highscore_index][1])

   tx = avg_magnitude * np.cos(avg_angle)
   ty = avg_magnitude * np.sin(avg_angle)

   #print("tx : %f, ty : %f" % (tx, ty))
   ##########################################################################
   return tx * 0.5, ty * 0.5, votes
