import numpy as np
import scipy.sparse as sparse
from scipy.sparse import linalg

"""
   Spectral clustering approximation to Normalized Cuts.

   Given an affinity matrix W specifying pairwise connection strengths between
   nodes in a graph, compute an embedding of the graph nodes into R^k, such
   that nodes belonging to the same cluster are close in the embedding space.

   Arguments:
      W     - an N x N numpy array, where N is the number of nodes in the graph
              Note: For efficiency, W should be sparse (most entries zero)
      k     - dimensionality of the output embedding space

   Returns:
      evec  - an N x k numpy array, mapping each node into R^k (feature vector
              of length k).
"""
def ncut(W, k = 8):
   # setup and solve generalized eigenproblem
   sw = W.shape[0]
   d = np.sum(W,1)
   ii = np.arange(sw)
   jj = np.arange(sw)
   D = sparse.csr_matrix((d, (ii,jj)), shape=(sw,sw))
   W = sparse.csr_matrix(W)
   ev, evec = linalg.eigs((D - W), M=D, k=(k+1), which='SM')
   # make real-valued
   ev = np.abs(ev)
   evec = np.abs(evec)
   # sort eigenvectors by importance
   ev_w = 1./(np.sqrt(ev) + np.finfo(float).eps)
   inds = np.argsort(-ev_w)
   ev_w = ev_w[inds[1:]]
   evec = evec[:,inds[1:]]
   # normalize and scale eigenvectors
   ev_w = ev_w / max(ev_w)
   for n in range(k):
      emin = np.amin(evec[:,n])
      emax = np.amax(evec[:,n])
      evec[:,n] = ev_w[n] * (evec[:,n] - emin) / (emax - emin + np.finfo(float).eps)
   return evec
