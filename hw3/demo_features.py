"""
   Demo: color and texture features
"""

import numpy as np
import matplotlib.pyplot as plt

from color import *
from textons import *
from util import *

# load example image
img = load_rgb_image('data/demo/69015.jpg')

# compute textons
txtn_idx = extract_textons(img)

# quantize colors in Lab colorspace
lab = rgb2lab_norm(img)
l_idx, a_idx, b_idx = quantize_lab(lab)

# display texton map and color quantization
plt.figure(); plt.imshow(img);                plt.title('image')
plt.figure(); plt.imshow(txtn_idx);           plt.title('texton map')
plt.figure(); plt.imshow(l_idx, cmap='gray'); plt.title('quantized l color channel')
plt.figure(); plt.imshow(a_idx, cmap='gray'); plt.title('quantized a color channel')
plt.figure(); plt.imshow(b_idx, cmap='gray'); plt.title('quantized b color channel')
plt.show()
