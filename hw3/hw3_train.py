# TODO: Your code here.
import numpy as np
import matplotlib.pyplot as plt

from superpixels import *
from ncut import *
from util import *

from hw3_lib import *

from sklearn import linear_model

from sklearn.externals import joblib #for exporting model

import os

data = []
target = []
for filename in os.listdir("data/train"):
    #filename = "94079.jpg"
    img = load_rgb_image("data/train/%s" % filename)
    seg = superpixels(img)
    print(filename)
    #print(seg)
    #adjacency_mx, region_pixels = seg2graph(seg)
    #region_colors = proj_onto_regions(region_pixels, img, 'mean')
    #seg_colors = render_onto_seg(seg, region_pixels, region_colors)
    #np.set_printoptions(threshold=np.inf)
    #print(len(adjacency_mx))
    #.png groundtruth files
    seg_gt = load_seg_gt('data/train_gt/%s.png' % os.path.splitext(filename)[0])
    adjacency_mx_gt, region_pixels_gt = seg2graph(seg_gt)
    region_colors_gt = proj_onto_regions(region_pixels_gt, img, 'mean')
    seg_colors_gt = render_onto_seg(seg_gt, region_pixels_gt, region_colors_gt)
    #print(len(region_pixels_gt[1]))
    super_pixels = []
    for i in range(len(region_pixels_gt)):
        temp = []
        for j in range(len(region_pixels_gt[i])):
            coord = np.unravel_index(region_pixels_gt[i][j], (img.shape[0], img.shape[1]))
            color = rgb2gray(img[coord[0]][coord[1]])
            temp.append(color)
            #print("%f %f" % (i, j))
        super_pixels.append(temp)
    #print(super_pixels)

    #print(seg_colors_gt)
    #print(len(seg_gt))
    #print(len(adjacency_mx_gt))
    #print(adjacency_mx_gt)
    #print(seg)
    #print(len(img[0]))
    """id_list = []
    spixel_dict = {}
    for i in range(len(seg)):
        for j in range(len(seg[0])):
            id = seg[i][j]
            if id not in id_list:
                id_list.append(id)
                spixel_dict[id] = []
            spixel_dict[id].append(img[i][j])
    np.set_printoptions(threshold=np.nan)
    print(adjacency_mx)"""
    #print(np.unravel_index(region_pixels[0][0], (img.shape[0], img.shape[1])))

    for i in range(0, len(super_pixels)):
        for j in range(0, len(super_pixels)):
            dist = calc_distance(sorted(super_pixels[i]), sorted(super_pixels[j]))
            data.append(dist)
            print("%f, %f" % (dist, adjacency_mx_gt[i][j]))
            if (dist > 2.0):
                target.append(0.0)
            else:
                target.append(adjacency_mx_gt[i][j])
    #seg_gt_proj = render_onto_seg(seg, region_pixels, region_gt)
    #plt.figure(); plt.imshow(img);         plt.title('image')
    #plt.figure(); plt.imshow(seg);         plt.title('superpixels')
    #plt.figure(); plt.imshow(seg_colors);  plt.title('mean color on superpixels')
    #plt.figure(); plt.imshow(seg_gt);      plt.title('groundtruth segmentation')
    #plt.figure(); plt.imshow(seg_gt_proj); plt.title('groundtruth on superpixels')
    #plt.show()
    break

train_data = np.array(data).reshape(-1, 1)
train_target = np.array(target)

log = linear_model.LogisticRegression(solver='lbfgs', C=1e5, multi_class='multinomial')
log.fit(train_data, train_target)

joblib.dump(log, 'LogClassifier7.joblib') #export classifier

"""
(A) Sample positive and negative pairs of adjacent superpixels.   (5 points)

 After transferring groundtruth region labels onto superpixels
 (see demo_superpixels.py), adjacent superpixels are a positive
 pair if they have the same groundtruth label, and a negative
 pair otherwise.
"""
#stored as adjacency_mx_gt where 1 is positive and 0 is negative

"""
(B) Extract features for these superpixel pairs.                  (5 points)

 A natural choice is to use the provided functions to compute textons
 and quantize color channels, and then extract texton and color
 histograms for each region.

 Crucially, a region pair could then be associated with a feature
 vector derived from measuring the distance between histograms.  The
 Chi-Squared distance is usually a good choice of distance measure
 between histograms.  (See lecture slides from Oct 12)
"""

"""
(C) Train a classifier to predict the probability that           (10 points)
 neighboring superpixels should be grouped together.
"""
