import numpy as np
import matplotlib.pyplot as plt

from superpixels import *
from ncut import *
from util import *
from hw3_lib import *

from sklearn import linear_model
from sklearn.externals import joblib #for importing model
import os

import math #for exp()

# given distance values array and classifier, get probability
# note that for log classifiers, "more negative" means smaller probability
def getProb(val, classifier):
    r = classifier.predict_log_proba(np.array(val).reshape(-1, 1))
    #print(r)
    probs = []
    max = math.exp(classifier.predict_log_proba(np.array([0]).reshape(-1, 1))[0][1])
    for i in range(len(r)):
        probs.append(math.exp(r[i][1]) / max)
    return probs

def merge_regions(region_pixels, to_merge):
    merged_region_pixels = []
    for i in range(len(to_merge)):
        temp = []
        for j in range(1, len(to_merge[i])): #dont merge with itself
            for px in region_pixels[to_merge[i][j]]:
                temp.append(px)
        merged_region_pixels.append(temp)
    return merged_region_pixels

def calc_distance(hist1, hist2):
    d = 0.5 * np.sum([((p1 - p2) * (p1 - p2)) / (p1 + p2 + 1e-10) for (p1, p2) in zip(hist1, hist2)])
    return d

def rgb2gray(rgb):
    gray = 0.2989 * rgb[0] + 0.5870 * rgb[1] + 0.1140 * rgb[2]
    return gray
