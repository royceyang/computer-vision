"""
   Texton features.
"""

import numpy as np
import scipy.signal
import scipy.cluster.vq

from color import rgb2gray
from filters import *

"""
   Compute a texton map for an RGB or grayscale image.

   The texton map is produced by the following procedure:

   (1) Convolve the image with a set of edge-like filters and use the result
       to create a feature vector for every pixel.

   (2) Perform k-means clustering on these feature vectors.

   (3) Assign each pixel an integer id corresponding to the cluster center
       closest to its feature descriptor.

   Arguments:
      img      - RGB or grayscale image as a numpy array
      n_bins   - number of texton bins (number of texton cluster centers)
      n_iter   - maximum number of iterations of k-means clustering
      filters  - filter set on which to base textons
                   (if None, then use that returned by texton_filters())

   Returns:
      txtn_idx - texton index map: a numpy array of the same spatial size as
                 the input image, where every pixel is assigned an integer
                 texton id in the range [0, n_bins-1]
"""
def extract_textons(img, n_bins = 16, n_iter = 100, filters = None):
   # convert image to grayscale
   if (img.ndim == 3):
      img = rgb2gray(img)
   assert img.ndim == 2, 'image should be grayscale'
   # check if using default texton filter set
   if filters is None:
      filters = texton_filters()
   # filter image
   fmaps = []
   for n in range(len(filters)):
      fmap = scipy.signal.convolve2d( \
         img, filters[n], mode = 'same', boundary = 'symm')
      fmaps.append(fmap)
   # concatenate filter responses
   fmaps = np.stack(fmaps, axis=-1)
   fmaps = fmaps.reshape((img.size, len(filters)))
   # k-means cluster
   centers, labels = scipy.cluster.vq.kmeans2( \
      fmaps, n_bins, iter = n_iter, minit = 'points')
   # reshape texton id map
   txtn_idx = labels.reshape(img.shape)
   return txtn_idx

"""
   Create a set of Gaussian derivative and center-surround filters for use in
   analysis of local patch texture.

   Arguments:
      sigma    - standard deviation of base Gaussian
      n_ori    - number of orientations to sample

   Returns:
      filters  - set of 2D oriented Gaussian derivative filters
"""
def texton_filters(sigma = 1.0, n_ori = 8):
   filters = []
   for n in range(n_ori):
      ori = n * (np.pi / n_ori)
      filters.append(gaussian_deriv_2d(sigma, 1, 0, ori))
      filters.append(gaussian_deriv_2d(sigma, 2, 0, ori))
   filters.append(gaussian_cs_2d(sigma))
   return filters
