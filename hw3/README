Homework 3 - Superpixels and Image Segmentation

Note: You may use existing python libraries in this assignment, and are not
      limited to using only numpy.

      In particular, using a library for learning logistic regression or
      another linear classification model is likely to be quite helpful.

      See, for example: sklearn.linear_model.LogisticRegression

      The only remaining restriction is that you may not call python library
      functions that perform image segmentation or otherwise defeat the
      purpose of this assignment.

In this assignment, you will build an image segmentation system by training a
classifier to predict whether or not neighboring superpixels belong to the same
region.

You are provided with:
 (*) Training data:
         data/train/*.jpg       - example images
         data/train_gt/*.png    - human-annotated groundtruth segmentations

     This data is from the Berkeley segmentation dataset.

     In util.py, see:
         load_rgb_image()
         load_seg_gt()

 (*) Texture and color feature extractors.
     See: demo_features.py

 (*) A superpixel segmentation procedure, as well as utility functions for
     building a graph representation of superpixels.
     See: demo_superpixels.py

 (*) A spectral clustering implementation.
     See: demo_ncut.py

You will need to implement training (hw3_train.py) and testing (hw3_test.py)
scripts, as well as any supporting functions of your choice (hw3_lib.py)

----------------------------------
PART I: Training (20 points total)

Your training script should iterate once over the training images, and for
each image:

   (A) Sample positive and negative pairs of adjacent superpixels.   (5 points)

       After transferring groundtruth region labels onto superpixels
       (see demo_superpixels.py), adjacent superpixels are a positive
       pair if they have the same groundtruth label, and a negative
       pair otherwise.

   (B) Extract features for these superpixel pairs.                  (5 points)

       A natural choice is to use the provided functions to compute textons
       and quantize color channels, and then extract texton and color
       histograms for each region.

       Crucially, a region pair could then be associated with a feature
       vector derived from measuring the distance between histograms.  The
       Chi-Squared distance is usually a good choice of distance measure
       between histograms.  (See lecture slides from Oct 12)

After assembling a large set of examples, you should then:

   (C) Train a classifier to predict the probability that           (10 points)
       neighboring superpixels should be grouped together.

----------------------------------
PART II: Testing (15 points total)

Apply your learned model to test images, then proceed to cluster and merge
superpixels into regions.

For each test image, your script should:

   (A) Apply your learned classifier to predict the probability      (5 points)
       that neighboring superpixels belong to the same region.

   (B) Transform these probabilities into an affinity matrix and     (5 points)
       use spectral clustering to embed superpixels into a
       feature space which encodes global similarity.
       (See demo_ncut.py)

   (C) Merge superpixels into regions.  You'll need to choose a      (5 points)
       threshold on similarity at which to stop merging.

--------------------------
Write-up (10 points total)

Briefly describe:
   (A) What features did you use?                                    (3 points)
   (B) What form of classifier did you use?                          (4 points)
       What was its accuracy on the training set?
   (C) Include a few (up to 10) example test images and              (3 points)
       the segmentations predicted by your system

----------------
Bonus (5 points)

Implement the ability to propagate constraints during merging of superpixels.
Specifically, given some subset of superpixels marked as foreground and another
subset marked as background, propagate foreground/background labels to all
unmarked superpixels.  Include example output in your write-up.

-------
Submit:
   hw3.pdf        - a write-up as described above
   hw3_train.py   - your training script implementation
   hw3_test.py    - your testing script implementation
   hw3_lib.py     - any supporting functions you wrote
   hw3_model      - (optional) a file storing parameters of your learned model
