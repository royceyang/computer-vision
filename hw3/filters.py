"""
   Utility functions for generating Gaussian and related filters.
   These filters can be useful in edge detection and texture analysis.
"""

import numpy as np
import scipy.ndimage

"""
   Return an approximation of a derviative of a 1-dimensional Gaussian filter.
   Specified derivative may be 0th, 1st, or 2nd order.

   The 1-dimensional Gaussian approximates:

      g(x) = 1 / sqrt(2 * pi * sigma^2) * exp( -(x^2) / (2 * sigma^2) )

   for x in the range [-3*sigma, 3*sigma]

   If requesting the 0th derivative, g(x) is returned, otherwise g'(x) or
   g''(x) is returned for the 1st or 2nd derivates, respectively.

   Arguments:
      sigma     - standard deviation
      deriv     - requested derivative (0, 1, or 2)
      min_width - minimum width of output filter

   Returns:
      filt      - requested 1D filter
"""
def gaussian_deriv_1d(sigma, deriv = 0, min_width = None):
   # compute x range
   if min_width is None:
      width = np.ceil(3.0 * sigma)
   else:
      width = np.ceil(max((3.0 * sigma), min_width))
   x = np.arange(-width, width + 1)
   # compute gaussian derivative
   x2                 = x * x
   sigma2_inv         = 1.0 / (sigma * sigma)
   neg_two_sigma2_inv = -0.5 * sigma2_inv
   if (deriv == 0):
      g = np.exp(x2 * neg_two_sigma2_inv)
   elif (deriv == 1):
      g = np.exp(x2 * neg_two_sigma2_inv) * (-x)
   elif (deriv == 2):
      g = np.exp(x2 * neg_two_sigma2_inv) * (x2 * sigma2_inv - 1)
   else:
      raise ValueError('deriv must be 0, 1, or 2')
   # make zero mean (if deriv > 0)
   if (deriv > 0):
      g = g - np.mean(g)
   # normalize
   filt = np.atleast_2d(g / np.sum(np.abs(g)))
   return filt

"""
   Return a rotated 2D Gaussian derivative filter.

   Arguments:
      sigma     - standard deviation
      deriv_x   - order of derivative in x-direction (0, 1, or 2)
      deriv_y   - order of derivative in y-direction (0, 1, or 2)
      ori       - orientation of filter (in radians)
      min_width - minimum width of output filter

   Returns:
      filt      - requested 2D filter
"""
def gaussian_deriv_2d( \
      sigma, deriv_x = 0, deriv_y = 0, ori = 0.0, min_width = None):
   gx = gaussian_deriv_1d(sigma, deriv_x, min_width)
   gy = gaussian_deriv_1d(sigma, deriv_y, min_width)
   filt = np.transpose(gx) * gy
   filt = scipy.ndimage.rotate(filt, (180.0 / np.pi) * ori, reshape=False)
   return filt

"""
   Return a 2D center-surround filter.

   Arguments:
      sigma     - standard deviation (of surround)

   Returns:
      filt      - requested 2D filter
"""
def gaussian_cs_2d(sigma):
   # compute center-surround filter
   width = np.ceil(3.0 * sigma)
   sigma_c = sigma / np.sqrt(2)
   f_c = gaussian_deriv_2d(sigma_c, 0, 0, 0.0, width)
   f_s = gaussian_deriv_2d(sigma,   0, 0, 0.0, width)
   filt = f_s - f_c
   # make zero mean and normalize
   filt = filt - np.mean(filt)
   filt = filt / np.sum(np.abs(filt))
   return filt
