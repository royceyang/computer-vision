# TODO: Your code here.
import numpy as np
import matplotlib.pyplot as plt

from superpixels import *
from ncut import *
from util import *
from hw3_lib import *

from sklearn import linear_model
from sklearn.externals import joblib #for importing model
import os

import math #for exp()


log = joblib.load('LogClassifier3.joblib') #import trained classifier
#print(getProb([0, 0.01, 0.5, 0.9, 3, 5, 100], log))
# make threshold = 99
thresh = .9875 #log classifier needs high threshold

#img = load_rgb_image('data/test/8068.jpg')
#img = load_rgb_image('data/test/36046.jpg')
#img = load_rgb_image('data/test/48025.jpg')
#img = load_rgb_image('data/test/100007.jpg')
#img = load_rgb_image('data/test/120003.jpg')
img = load_rgb_image('data/test/181021.jpg')

seg = superpixels(img)
adjacency_mx, region_pixels = seg2graph(seg)

super_pixels = []
for i in range(len(region_pixels)):
    temp = []
    for j in range(len(region_pixels[i])):
        coord = np.unravel_index(region_pixels[i][j], (img.shape[0], img.shape[1]))
        color = rgb2gray(img[coord[0]][coord[1]])
        temp.append(color)
        #print("%f %f" % (i, j))
    super_pixels.append(temp)

prob_mx = []
for i in range(len(super_pixels)):
    temp = []
    for j in range(len(super_pixels)):
        temp.append(getProb(calc_distance(super_pixels[i], super_pixels[j]), log))
    prob_mx.append(temp)
#print(prob_mx)

to_merge = []

# index regions to merge; condition: regions must have similarity higher than threshold
for i in range(len(prob_mx)):
    for j in range(len(prob_mx[i])):
        if (prob_mx[i][j][0] > thresh):
            newele = True
            for ele in to_merge:
                if i in ele and j not in ele:
                    ele.append(j)
                    newele = False
                    break
                elif j in ele and i not in ele:
                    ele.append(i)
                    newele = False
                    break
                elif i in ele and j in ele:
                    newele = False
                    break
            if newele:
                to_merge.append([i, j])

#print(len(prob_mx))
#print(len(region_pixels))
#print(len(to_merge))
#print(to_merge)


merged_region_pixels = merge_regions(region_pixels, to_merge)
#print(len(region_pixels))
#print(len(merged_region_pixels))


region = proj_onto_regions(merged_region_pixels, seg, 'mode')

noise_level = 0.5
n_reg = region.size
W = np.zeros((n_reg, n_reg))
for r0 in range(n_reg):
   for r1 in range(n_reg):
      noise = noise_level * np.random.rand(1);
      W[r0,r1] = (region[r0] == region[r1]) * (1.0 - noise_level) + noise

k = 8
evec = ncut(W, k)

evec012 = render_onto_seg(seg, merged_region_pixels, evec[:,0:3])
evec345 = render_onto_seg(seg, merged_region_pixels, evec[:,3:6])

plt.figure(); plt.imshow(evec012);
plt.title('First 3 dimensions of region feature embedding');
plt.figure(); plt.imshow(evec345)
plt.title('Dimensions 4-6 of region feature embedding');
plt.show()
