"""
   Color space conversion and quantization utilities.
"""

import numpy as np
import skimage.color

"""
   Convert an RGB image to grayscale.

   This function applies a fixed weighting of the color channels to form the
   resulting intensity image.

   Arguments:
      rgb  - 3D numpy array of shape (sx, sy, 3) storing an RGB image

   Returns:
      gray - 2D numpy array of shape (sx, sy) storing the corresponding
             grayscale image
"""
def rgb2gray(rgb):
    gray = np.dot(rgb[...,:3],[0.29894, 0.58704, 0.11402])
    return gray

"""
   Convert an RGB image to normalized Lab color space.

   Lab is a perceptually uniform color space.  Normalized Lab color space
   rescales the range of each of the l, a, and b channels to the interval [0,1].

   Arguments:
      rgb - 3D numpy array of shape (sx, sy, 3) storing an RGB image

   Returns:
      lab - 3D numpy array of shape (sx, sy, 3) storing a normalized Lab image
"""
def rgb2lab_norm(rgb):
   # convert to lab
   lab = skimage.color.rgb2lab(rgb);
   l = lab[:,:,0]
   a = lab[:,:,1]
   b = lab[:,:,2]
   # range bounds
   ab_min = -73
   ab_max = 95
   ab_range = ab_max - ab_min;
   # normalize
   l = l / 100
   a = (a - ab_min) / ab_range
   b = (b - ab_min) / ab_range
   # clip
   l = np.maximum(np.minimum(l,1),0)
   a = np.maximum(np.minimum(a,1),0)
   b = np.maximum(np.minimum(b,1),0)
   lab = np.stack((l,a,b), axis=2)
   return lab

"""
   Quantize color channels of an image in normalized Lab color space.

   Arguments:
      lab      - 3D numpy array storing a normalized Lab image
      n_l_bins - number of bins to use for l channel
      n_a_bins - number of bins to use for a channel
      n_b_bins - number of bins to use for b channel

   Returns:
      l_idx    - bin index map for l channel
      a_idx    - bin index map for a channel
      b_idx    - bin index map for b channel
"""
def quantize_lab(lab, n_l_bins = 8, n_a_bins = 8, n_b_bins = 8):
   # extract l, a, b channels
   l = lab[:,:,0]
   a = lab[:,:,1]
   b = lab[:,:,2]
   # quantize
   l_idx = np.minimum(np.floor(l * n_l_bins), (n_l_bins-1))
   a_idx = np.minimum(np.floor(a * n_a_bins), (n_a_bins-1))
   b_idx = np.minimum(np.floor(b * n_b_bins), (n_b_bins-1))
   return l_idx, a_idx, b_idx
