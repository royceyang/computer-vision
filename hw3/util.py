import scipy.misc

"""
   Load an RGB image.

   Arguments:
      filename - image file to load

   Returns:
      image    - a 3D numpy array containing an RGB image
"""
def load_rgb_image(filename):
   image = scipy.misc.imread(filename)
   image = image / 255;
   return image

"""
   Load groundtruth segmentation.

   Arguments:
      filename - groundtruth file to load

   Returns:
      seg_gt   - a 2D numpy array containing a region id for each pixel
"""
def load_seg_gt(filename):
   seg_gt = scipy.misc.imread(filename)
   return seg_gt
